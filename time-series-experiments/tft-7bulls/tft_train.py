import sys
sys.path.append("../")

import ast
import torch
import click
import pytorch_lightning as pl
from pytorch_lightning.callbacks import EarlyStopping, LearningRateMonitor
from pytorch_forecasting.metrics import QuantileLoss, MAE, Dilate, RMSE, MASE
from pytorch_forecasting import TimeSeriesDataSet, TemporalFusionTransformer
import pandas as pd
from pytorch_forecasting.data import NaNLabelEncoder
from pytorch_lightning.loggers.neptune import NeptuneLogger
from experiments_utils import test_model


@click.command()
@click.option("-csv_path", default="data.csv", help="path to csv")
@click.option("-bs", default=64,help="batch size")
@click.option("-max_epochs", default=100, help="max epochs")
@click.option("-target_column", default='value')
@click.option("-tv_unknown_reals", default='[]',
              help="time varying unknown reals, other than target")
@click.option("-known_reals", default='[]',
              help="time varying known reals, other than target")
@click.option("-tv_unknown_cat", default='[]',
              help="time varying unknown categoricals, other than target")
@click.option("-seed", default=1235, help="torch seed")
@click.option("-lr", default=0.05, help="learning rate")
@click.option("-loss", default='quantile', help="loss function from: quantile, dilate, rmse, mae")
@click.option("-tags", default='["tft", "imputed", "v2"]', help="tags for neptune logger")
@click.option("-model_path", default='./', help="path to saved model")
@click.option("-context_length", default=50, help="context length")
@click.option("-prediction_length", default=10, help="pprediction length")
@click.option("-hidden_size", default=32)
@click.option("-attention_head_size", default=1)
@click.option("-hidden_continuous_size", default=16)
@click.option("-output_size", default=7)
@click.option("-mode", default='fcr', help="fcr or genome")
def main(csv_path, bs, max_epochs, target_column, tv_unknown_reals, tv_unknown_cat, known_reals, seed, lr, loss, tags
         ,model_path, context_length, prediction_length, hidden_size, attention_head_size, hidden_continuous_size,
         output_size, mode):
    params = locals().copy()
    torch.manual_seed(seed)

    tv_unknown_reals = ast.literal_eval(tv_unknown_reals)
    tv_unknown_cat = ast.literal_eval(tv_unknown_cat)
    known_reals = ast.literal_eval(known_reals)

    tags = ast.literal_eval(tags)

    dataset = pd.read_csv(csv_path)
    dataset[target_column] = dataset[target_column].astype(float)
    print(dataset)

    for name in tv_unknown_cat:
        print(tv_unknown_cat)
        dataset[name] = dataset[name].astype(str)

    n = dataset.shape[0]
    print(n)

    loss_dict = {
        'quantile': QuantileLoss(),
        'dilate': Dilate(),
        'mae': MAE(),
        'rmse': RMSE()
    }

    neptune_logger = NeptuneLogger(
        api_key="eyJhcGlfYWRkcmVzcyI6Imh0dHBzOi8vdWkubmVwdHVuZS5haSIsImFwaV91cmwiOiJodHRwczov"
                "L3VpLm5lcHR1bmUuYWkiLCJhcGlfa2V5IjoiMjkwZWQ4ZWItZTZhOC00OTg4LWFkYjctYTkyNTc3ODMwNTg1In0=",
        project_name="Morphemic/sandbox",
        params=params,
        tags=tags,
        close_after_fit=False,

    )

    max_time = dataset.time_idx.max()

    training = TimeSeriesDataSet(
        dataset[lambda x: x.time_idx < int(max_time * 0.7)],
        time_idx="time_idx",
        target=target_column,
        group_ids=["series"],
        min_encoder_length=context_length,  # keep encoder length long (as it is in the validation set)
        max_encoder_length=context_length,
        min_prediction_length=prediction_length,
        max_prediction_length=prediction_length,
        static_categoricals=[],
        static_reals=[],
        time_varying_known_categoricals=[],
        categorical_encoders={"series": NaNLabelEncoder().fit(dataset.series)},
        variable_groups={},  # group of categorical variables can be treated as one variable
        time_varying_known_reals=["time_idx"] + known_reals,
        time_varying_unknown_categoricals=tv_unknown_cat,
        time_varying_unknown_reals=[target_column] + tv_unknown_reals,
        add_relative_time_idx=True,
        add_target_scales=True,
        add_encoder_length=True,
    )

    validation = TimeSeriesDataSet.from_dataset(training, dataset[lambda x: x.series == 0],
                                                min_prediction_idx=training.index.time.max() + 1,
                                                stop_randomization=True)

    test = TimeSeriesDataSet.from_dataset(training, dataset[lambda x: x.series == 0],
                                                min_prediction_idx=training.index.time.max() + 1 + int(n * 0.2),
                                                stop_randomization=True)

    train_dataloader = training.to_dataloader(train=True, batch_size=bs, num_workers=6)
    val_dataloader = validation.to_dataloader(train=False, batch_size=bs, num_workers=6)
    test_dataloader = test.to_dataloader(train=False, batch_size=bs, num_workers=6)

    early_stop_callback = EarlyStopping(monitor="val_loss", min_delta=1e-5, patience=8, verbose=False, mode="min")
    lr_logger = LearningRateMonitor()
    trainer = pl.Trainer(
        max_epochs=max_epochs,
        gpus=1,
        gradient_clip_val=0.16,
        callbacks=[lr_logger, early_stop_callback],
        logger=neptune_logger
    )

    tft = TemporalFusionTransformer.from_dataset(
        training,
        learning_rate=lr,
        hidden_size=hidden_size,
        attention_head_size=attention_head_size,
        dropout=0.1,
        hidden_continuous_size=hidden_continuous_size,
        output_size=output_size,
        loss=loss_dict[loss],
        log_interval=1000,
        reduce_on_plateau_patience=5
    )

    trainer.fit(
        tft, train_dataloader=train_dataloader,
        val_dataloaders=val_dataloader,
    )

    if mode == 'fcr':
        test_model(tft, trainer, dataset, test_dataloader, training,
                   neptune_logger, context_length, prediction_length, plot_preds=True)
    elif mode == 'genome':
        test_model(tft, trainer, dataset, test_dataloader, training,
                   neptune_logger, context_length, prediction_length, plot_steps=True)

if __name__ == '__main__':
    main()
