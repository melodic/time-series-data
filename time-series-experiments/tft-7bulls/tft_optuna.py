import sys
sys.path.append("..")

import ast
import torch
import click
from pytorch_forecasting import TimeSeriesDataSet, TemporalFusionTransformer
import pandas as pd
from pytorch_forecasting.data import NaNLabelEncoder
import pickle
from pytorch_forecasting.models.temporal_fusion_transformer.tuning import optimize_hyperparameters
import json
import os

@click.command()
@click.option("-csv_path", default="data.csv", help="path to csv")
@click.option("-n_trails", default=150,help="n trials")
@click.option("-max_epochs", default=30,help="max epochs")
@click.option("-bs", default=64,help="batch size")
@click.option("-target_column", default='value')
@click.option("-tv_unknown_reals", default='[]',
              help="time varying unknown reals, other than target")
@click.option("-known_reals", default='[]',
              help="time varying unknown reals, other than target")
@click.option("-tv_unknown_cat", default='["timeStamp"]',
              help="time varying unknown categoricals, other than target")
@click.option("-seed", default=1234, help="torch seed")
@click.option("-model_path", default='./', help="path for saved model")
@click.option("-context_length", default=400, help="context length")
@click.option("-prediction_length", default=31, help="pprediction length")
@click.option("-best_params_path", default='./', help="path for json with the best hyperparameters")
def main(csv_path, n_trails, max_epochs, bs, target_column, tv_unknown_reals, tv_unknown_cat, known_reals, seed
         ,model_path, context_length, prediction_length, best_params_path):
    torch.manual_seed(seed)

    dataset = pd.read_csv(csv_path)

    tv_unknown_reals = ast.literal_eval(tv_unknown_reals)
    tv_unknown_cat = ast.literal_eval(tv_unknown_cat)
    known_reals = ast.literal_eval(known_reals)

    dataset = pd.read_csv(csv_path)
    dataset[target_column] = dataset[target_column].astype(float)
    print(dataset)

    for name in tv_unknown_cat:
        print(tv_unknown_cat)
        dataset[name] = dataset[name].astype(str)

    n = dataset.shape[0]
    train, val, test = dataset.head(int(n * 0.7)), dataset.iloc[-int(n * 0.3):-int(n * 0.1)], dataset.tail(int(n * 0.1))

    training = TimeSeriesDataSet(
        dataset[lambda x: x.time_idx <= train.shape[0]],
        time_idx="time_idx",
        target=target_column,
        group_ids=["series"],
        min_encoder_length=context_length,  # keep encoder length long (as it is in the validation set)
        max_encoder_length=context_length,
        min_prediction_length=prediction_length,
        max_prediction_length=prediction_length,
        static_categoricals=[],
        static_reals=[],
        time_varying_known_categoricals=[],
        categorical_encoders={"series": NaNLabelEncoder().fit(dataset.series)},
        variable_groups={},  # group of categorical variables can be treated as one variable
        time_varying_known_reals=["time_idx"] + known_reals,
        time_varying_unknown_categoricals=tv_unknown_cat,
        time_varying_unknown_reals=[target_column] + tv_unknown_reals,
        add_relative_time_idx=True,
        add_target_scales=True,
        add_encoder_length=True,
        allow_missings=True
    )

    validation = TimeSeriesDataSet.from_dataset(training, dataset.head(int(n * 0.9)),
                                                min_prediction_idx=training.index.time.max() + 1,
                                                stop_randomization=True)

    train_dataloader = training.to_dataloader(train=True, batch_size=bs, num_workers=6)
    val_dataloader = validation.to_dataloader(train=False, batch_size=bs, num_workers=6)

    # create study
    study = optimize_hyperparameters(
        train_dataloader,
        val_dataloader,
        model_path=model_path,
        n_trials=n_trails,
        max_epochs=max_epochs,
        gradient_clip_val_range=(0.01, 1.0),
        hidden_size_range=(6, 128),
        hidden_continuous_size_range=(6, 128),
        attention_head_size_range=(1, 4),
        learning_rate_range=(0.001, 0.1),
        dropout_range=(0.01, 0.3),
        reduce_on_plateau_patience=4,
        use_learning_rate_finder=False,  # use Optuna to find ideal learning rate or use in-built learning rate finder
    )

    # save study results - also we can resume tuning at a later point in time
    with open("test_study.pkl", "wb") as fout:
        pickle.dump(study, fout)

    with open(os.path.join(best_params_path, 'best_params.json'), "w") as fp:
        json.dump(study.best_trial.params, fp)

        # show best hyperparameters
    print('BEST HYPERAPAREMTERS', study.best_trial.params)

if __name__ == '__main__':
    main()






















