from pytorch_forecasting.visualisation.custom_plots import plot_interval_predictions, plot_real_points_pred
from pytorch_forecasting import TimeSeriesDataSet, TemporalFusionTransformer

def test_model(tft, trainer, dataset, test_dataloader, training, neptune_logger,
               context_length, prediction_length, plot_steps=False, plot_preds=True):
    print('testing')

    best_model_path = trainer.checkpoint_callback.best_model_path
    best_tft = TemporalFusionTransformer.load_from_checkpoint(best_model_path)

    trainer.test(model=tft, test_dataloaders=test_dataloader)

    new_loader = TimeSeriesDataSet.from_dataset(training, dataset,
                                                min_prediction_idx=0,
                                                stop_randomization=True)

    if plot_steps:
        plot_interval_predictions(best_tft, new_loader, context_length, prediction_length, neptune_logger)

    raw_predictions, x = best_tft.predict(new_loader, mode="raw", return_x=True)
    interpretation = best_tft.interpret_output(raw_predictions, reduction="sum")
    figures = best_tft.plot_interpretation(interpretation)
    for fig in figures:
        neptune_logger.log_image(f'{fig}', figures[fig])

    if plot_preds:
        plot_real_points_pred(best_tft, new_loader, context_length, prediction_length, neptune_logger)

    print('done!')
