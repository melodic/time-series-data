# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import os


ROOT_DATA_PATH = "../../../../../time-series-data/secure-document​/deployment-reconfiguration-range-2-to-2/2020-10-26 to 2020-10-26/V1.0 - raw data"
TARGET_FILE = "AvgResponseTimeTable (kopia).csv"
TARGET_PATH = os.path.join(ROOT_DATA_PATH, TARGET_FILE)

REQUESTS_FILE = "jmeterResults (kopia).csv"
REQUESTS_PATH = os.path.join(ROOT_DATA_PATH, REQUESTS_FILE)

TIME_COLUMN = 'time'
TARGET_COLUMN = 'value'
REQUEST_COLUMN = 'timeStamp'

PREPROCESS_DATA_PATH = './data.csv'

def main():
    data = pd.read_csv(TARGET_PATH)
    data = data.tail(data.shape[0] - 11) #skipping past dates
    data[TIME_COLUMN] = pd.to_datetime(data[TIME_COLUMN]).dt.tz_localize(None)

    requests = pd.read_csv(REQUESTS_PATH)
    requests[REQUEST_COLUMN] = pd.to_datetime(requests[REQUEST_COLUMN]).dt.tz_localize(None)
    requests = requests[requests[REQUEST_COLUMN] > data.iloc[0][TIME_COLUMN]]

    requests = pd.DataFrame({TIME_COLUMN: requests[REQUEST_COLUMN]})
    requests[TARGET_COLUMN] = np.nan
    requests[REQUEST_COLUMN] = 1

    data = data[[TIME_COLUMN, TARGET_COLUMN]]
    data[REQUEST_COLUMN] = 0

    tmp_data = pd.concat([data, requests])
    tmp_data = tmp_data.sort_values(by=[TIME_COLUMN])
    tmp_data.index = tmp_data[TIME_COLUMN]

    final_data = tmp_data.assign(InterpolateTime=tmp_data[TARGET_COLUMN].interpolate(method='time')) #imputation
    final_data.index = range(final_data.shape[0])
    final_data[TARGET_COLUMN] = final_data['InterpolateTime']
    del final_data['InterpolateTime']

    final_data[REQUEST_COLUMN] = final_data[REQUEST_COLUMN].astype(str)
    start_date = pd.to_datetime(final_data[TIME_COLUMN][0]).value
    final_data[TIME_COLUMN] = pd.Series([pd.to_datetime(x).value - start_date for x in final_data[TIME_COLUMN].values])
    final_data['time_idx'] = range(final_data.shape[0])
    final_data['series'] = 0

    final_data.to_csv(PREPROCESS_DATA_PATH)

if __name__ == '__main__':
    main()