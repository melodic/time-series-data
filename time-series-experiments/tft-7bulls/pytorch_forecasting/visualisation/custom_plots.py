import torch
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches
from scipy.sparse import csr_matrix, hstack, vstack


def smape(A, F):
    print(len(A))
    return 1/len(A) * (np.sum(2 * np.abs(F - A) / (np.abs(A) + np.abs(F) + 0.000001)))

def create_title(fig, preds, reals, message=''):
    smape_scores = [f'{ds} : {round(smape(reals[j], preds[j]), 3)}' for j, ds in enumerate(['train', 'val', 'test'])]
    fig.suptitle(f'predictions smape: ' + ', '.join(smape_scores) + message)

def plot_interval_predictions(best_tft, loader, context_length, pred_len, neptune_logger):
    colors = ['r', 'g', 'y']

    actuals = torch.stack([y for _, y in iter(loader)])
    predictions = best_tft.predict(loader)
    preds, reals = predictions.numpy(), actuals.numpy()
    print('preds', preds.shape, 'reals', reals.shape)
    # preds, reals = 1 / preds - 1, 1 / reals - 1
    preds, reals = np.where(preds==0, 0.0001, preds), np.where(reals==0, 0.0001, reals) # in order to use sparse matrix

    for i in range(pred_len):
        if i % 1 == 0 and i in range(0, 10):
            fig, ax = plt.subplots(2, figsize=(25, 5))
            ax[1].set_xlabel('time')
            plot_predictions(fig, ax, colors, pred_len, i, preds, reals)
            labels = [mpatches.Patch(color=c, label=l) for c, l in zip(['blue'] + colors, ['real', 'train', 'val', 'test'])]
            plt.legend(handles=labels)
            neptune_logger.log_image(f'predictions_steps', fig)

    n_predictions = predictions.shape[0] + pred_len - 1

    preds_with_nan = [csr_matrix(np.concatenate((np.full(j, 0), p, np.full(max(n_predictions - j - pred_len, 0), 0)),
                                     axis=0)) for j, p in enumerate(preds)]
    reals_with_nan = [csr_matrix(np.concatenate((np.full(j, 0), p, np.full(max(n_predictions - j - pred_len, 0), 0)),
                                     axis=0)) for j, p in enumerate(reals)]

    preds_with_nan, reals_with_nan = vstack(preds_with_nan), vstack(reals_with_nan)

    x = preds_with_nan.sum(axis=0) / preds_with_nan.getnnz(axis=0)
    y = reals_with_nan.sum(axis=0) / reals_with_nan.getnnz(axis=0)

    x, y = np.squeeze(np.asarray(x)), np.squeeze(np.asarray(y))

    fig, ax = plt.subplots(2, figsize=(25, 5))
    ax[0].set_xlabel('time')
    ax[0].set_ylabel('Target')
    create_plot(fig, ax, x, y, pred_len, colors)
    labels = [mpatches.Patch(color=c, label=l) for c, l in zip(['blue'] + colors, ['real', 'train', 'val', 'test'])]
    plt.legend(handles=labels)
    neptune_logger.log_image(f'predictions_steps', fig)
    #
    # np.save('preds_17.npy', x)
    # np.save('reals_17.npy', y)
    # print('saved')

    fig, ax = plt.subplots(2, figsize=(25, 5))
    ax[0].set_xlabel('time')
    ax[0].set_ylabel('Target')
    create_stacked_plot(fig, ax, x, y, pred_len, colors)
    labels = [mpatches.Patch(color=c, label=l) for c, l in zip(['blue'] + colors, ['real', 'train', 'val', 'test'])]
    plt.legend(handles=labels)
    neptune_logger.log_image(f'predictions_stacked', fig)

    fig, ax = plt.subplots(2, figsize=(25, 5))
    ax[0].set_xlabel('time')
    ax[0].set_ylabel('Target')
    ax[0].plot(list(range(y.shape[0])), y)
    # labels = [mpatches.Patch(color=c, label=l) for c, l in zip(['blue'] + colors, ['real', 'train', 'val', 'test'])]
    # plt.legend(handles=labels)
    neptune_logger.log_image(f'original plot', fig)



def create_stacked_plot(fig, ax, x, y, i, colors):
    fig.suptitle(f'mean step predictions smape: {smape(np.array(x), np.array(y))}')

    n = len(x)
    split_n = [int(n * 0.7) - i, int(n * 0.9), n]

    predictions, actuals = np.split(x, split_n), np.split(y, split_n)

    create_title(fig, predictions, actuals)

    for pred, real, color in zip(predictions, actuals, colors):
        x = np.arange(pred.shape[0])
        ax[0].plot(x, pred, color)


def create_plot(fig, ax, x, y, i, colors):
    n = len(x)
    split_n = [int(n * 0.7) - i, int(n * 0.9), n]

    predictions, actuals = np.split(x, split_n), np.split(y, split_n)

    create_title(fig, predictions, actuals, message='mean')

    start_idx = 0

    for pred, real, color in zip(predictions, actuals, colors):
        x = np.arange(start_idx, start_idx + pred.shape[0])
        start_idx = start_idx + x.shape[0]
        ax[0].plot(x, real, 'b')
        ax[0].plot(x, pred, color)
        ax[1].plot(x, real - pred, color)

    # ax.grid(True)

def plot_predictions(fig, ax, colors, pred_len, i, predictions, actuals):
    assert i < pred_len, 'i > pred_len'

    predictions = np.array([pred[i] for pred in predictions])
    actuals = np.array([real[i] for real in actuals])

    n = len(predictions)
    split_n = [int(n * 0.7) - i, int(n * 0.9), n]

    slit_predictions, split_actuals = np.split(predictions, split_n), np.split(actuals, split_n)
    start_idx = 0

    create_title(fig, slit_predictions, split_actuals, message=f'step {i}')

    for pred, real, color in zip(slit_predictions, split_actuals, colors):
        x = np.arange(start_idx, start_idx + pred.shape[0])
        start_idx = start_idx + x.shape[0]
        ax[0].plot(x, real, 'b')
        ax[0].plot(x, pred, color)
        ax[1].plot(x, real - pred, color)
    # ax.xaxis.set_ticks(np.arange(0, predictions.shape[0], i + 1))
    # ax.grid(True)


def plot_real_points_pred(tft, loader, context_length, pred_len, neptune_logger):
    colors = ['r', 'g', 'y']

    # actuals = torch.stack([y for x, y in iter(loader) if x['x_cat'][-1][0].item() == 1])
    actuals = torch.stack([y for x, y in iter(loader)])

    predictions, reals = tft.predict(loader, real_points_only=True)

    predictions = [p for p, r in zip(predictions, reals) if r[1] == 1]
    reals = [a for a, r in zip(actuals, reals) if r[1] == 1]

    predictions = torch.stack(predictions)
    reals = torch.stack(reals)

    print(predictions[0].shape)

    preds, reals = predictions.numpy(), reals.numpy()
    # preds, reals = 1 / preds - 1, 1 / reals - 1

    preds = [p[:pred_len] for p in preds]
    reals = [r[:pred_len] for r in reals]

    preds, reals = np.concatenate(preds), np.concatenate(reals)

    fig, axs = plt.subplots(2, figsize=(25, 5))
    axs[0].set_ylabel('AvgResponseTime')
    axs[0].set_xlabel('time')
    axs[0].set_ylabel('predictions')
    axs[1].set_ylabel('residuals')

    create_plot(fig, axs, preds, reals, 0, ['r', 'g', 'y'])

    labels = [mpatches.Patch(color=c, label=l) for c, l in zip(colors, ['train', 'val', 'test'])]
    plt.legend(handles=labels)
    neptune_logger.log_image(f'predictions_reals_only', fig)


color_dict = {'train': 'red', 'val': 'green', 'test': 'yellow'}



def plot_predictions_gif(x, y, t, pred_len, path):
    os.makedirs(path)
    start = 0
    print(t)
    n = len(x) * pred_len
    # fig, ax=plt.subplots(figsize=(20, 5))
    # ax.set_xlim(0, n)
    # ax.set_ylim(0, 100)
    for i, p, r, time in zip(range(n), x, y, t):
        fig, ax=plt.subplots(2, figsize=(15, 5))
        ax[0].set_xlim(0, n)
        ax[0].set_ylim(0, 100)
        color = dataset.df['split'][time[0]]
        if time[0] < max(np.array(t).flatten()) * 0.7:
            color = 'train'
        elif time[0] < max(np.array(t).flatten()) * 0.9:
            color = 'val'
        else:
            color = 'test'

        print(color)
        ind = np.array(range(np.array(x[:i]).flatten().shape[0]))
        segments_x = [[k, k + 1] for k in  ind - 1]
        preds_x = np.array(x[:i]).flatten()
        segments_y = [[preds_x[k], preds_x[k + 1]] for k in  ind - 1]

        linecolors = []
        m = np.array(x[:i]).flatten().shape[0]
        for j in range(m):
            if j < n * 0.7:
                linecolors.append('red')
            elif j < n * 0.9:
                linecolors.append('green')
            else:
                linecolors.append('yellow')
            
        segments = [[x_, y_] for x_, y_ in zip(segments_x, segments_y)]
        for s, c in zip(segments, linecolors):
            ax[0].plot(s[0], s[1], color=c)

        # ax[0].plot(np.array(x[:i]).flatten(), color=color_dict[color], label=color)
        ax[0].plot(np.array(y[:i]).flatten(),  color='blue', label='real')
        ax[0].axvspan(start, start + pred_len, facecolor='0.1', alpha=0.2)

        ax[0].plot(range(start, start + pred_len), p, color=color_dict[color], label=color)
        ax[0].plot(range(start, start + pred_len), r, color='blue')

        ax[1].set_xlim(0, n)
        ax[1].set_ylim(-20, 500)
        ax[1].plot(np.array(x[:i]).flatten() - np.array(y[:i]).flatten(), color='red', label='residuals')
        ax[1].axvspan(start, start + pred_len, facecolor='0.1', alpha=0.2)

        ax[1].plot(range(start, start + pred_len), np.array(p) - np.array(r), color='red')
        start += pred_len
        red_patch = mpatches.Patch(color='red', label='train')
        green_patch = mpatches.Patch(color='green', label='val')
        yellow_patch = mpatches.Patch(color='yellow', label='test')

        ax[0].legend(handles=[red_patch, green_patch, yellow_patch, mpatches.Patch(color='blue', label='real')])
        ax[1].legend()
        ax[1].legend()
        fig.suptitle('Predictions results')
        plt.tight_layout()
        if i <= 9:
            fig.savefig(f'{path}/00{i}.png')
        else:
            fig.savefig(f'{path}/0{i}.png')






