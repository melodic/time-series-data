import sys
sys.path.append("../")

import ast
import torch
import click
import pytorch_lightning as pl
from pytorch_lightning.callbacks import EarlyStopping, LearningRateMonitor
from pytorch_forecasting import TimeSeriesDataSet, NBeats
import pandas as pd
from pytorch_forecasting.data import NaNLabelEncoder
from pytorch_lightning.loggers.neptune import NeptuneLogger
from experiments_utils import test_model
from pytorch_forecasting.visualisation.custom_plots import plot_interval_predictions, plot_real_points_pred
from pytorch_forecasting.metrics import QuantileLoss, MAE, Dilate, RMSE, MASE


@click.command()
@click.option("-csv_path", default="data.csv", help="path to csv")
@click.option("-bs", default=128,help="batch size")
@click.option("-max_epochs", default=100, help="max epochs")
@click.option("-target_column", default='TimeStamp')
@click.option("-tv_unknown_reals", default='[]',
              help="time varying unknown reals, other than target")
@click.option("-known_reals", default='[]',
              help="time varying unknown reals, other than target")
@click.option("-tv_unknown_cat", default='[]',
              help="time varying unknown categoricals, other than target")
@click.option("-seed", default=154, help="torch seed")
@click.option("-lr", default=0.05, help="learning rate")
@click.option("-tags", default='["tft", "imputed", "v2"]', help="tags for neptune logger")
@click.option("-model_path", default='./', help="path to saved model")
@click.option("-context_length", default=50, help="context length")
@click.option("-prediction_length", default=32, help="pprediction length")
@click.option("-hidden_size", default=32)
@click.option("-attention_head_size", default=1)
@click.option("-hidden_continuous_size", default=16)
@click.option("-output_size", default=7)
def main(csv_path, bs, max_epochs, target_column, tv_unknown_reals, tv_unknown_cat, known_reals, seed, lr, tags
         ,model_path, context_length, prediction_length, hidden_size, attention_head_size, hidden_continuous_size,
         output_size):
    params = locals().copy()
    torch.manual_seed(seed)

    tv_unknown_reals = ast.literal_eval(tv_unknown_reals)
    tv_unknown_cat = ast.literal_eval(tv_unknown_cat)
    known_reals = ast.literal_eval(known_reals)

    tags = ast.literal_eval(tags)

    dataset = pd.read_csv(csv_path)
    dataset[target_column] = dataset[target_column].astype(float)
    print(dataset)

    for name in tv_unknown_cat:
        print(tv_unknown_cat)
        dataset[name] = dataset[name].astype(str)

    n = dataset.shape[0]

    neptune_logger = NeptuneLogger(
        api_key="eyJhcGlfYWRkcmVzcyI6Imh0dHBzOi8vdWkubmVwdHVuZS5haSIsImFwaV91cmwiOiJodHRwczov"
                "L3VpLm5lcHR1bmUuYWkiLCJhcGlfa2V5IjoiMjkwZWQ4ZWItZTZhOC00OTg4LWFkYjctYTkyNTc3ODMwNTg1In0=",
        project_name="Morphemic/sandbox",
        params=params,
        tags=tags + ['nbeats'],
        close_after_fit=False,

    )

    training = TimeSeriesDataSet(
        dataset[lambda x: x.time_idx < int(n * 0.7)],
        time_idx="time_idx",
        target=target_column,
        categorical_encoders={"series": NaNLabelEncoder().fit(dataset.series)},
        group_ids=["series"],
        min_encoder_length=context_length,
        max_encoder_length=context_length,
        max_prediction_length=prediction_length,
        min_prediction_length=prediction_length,
        time_varying_unknown_reals=[target_column],
        randomize_length=None,
        add_relative_time_idx=False,
        add_target_scales=False,
    )

    validation = TimeSeriesDataSet.from_dataset(training, dataset.head(int(n * 0.9)),
                                                min_prediction_idx=training.index.time.max() + 1,
                                                stop_randomization=True)

    test = TimeSeriesDataSet.from_dataset(training, dataset,
                                                min_prediction_idx=training.index.time.max() + 1 + int(n * 0.2),
                                                stop_randomization=True)

    train_dataloader = training.to_dataloader(train=True, batch_size=bs, num_workers=6)
    val_dataloader = validation.to_dataloader(train=False, batch_size=bs, num_workers=6)
    test_dataloader = test.to_dataloader(train=False, batch_size=bs, num_workers=6)

    early_stop_callback = EarlyStopping(monitor="val_loss", min_delta=1e-5, patience=5, verbose=False, mode="min")
    lr_logger = LearningRateMonitor()
    trainer = pl.Trainer(
        max_epochs=max_epochs,
        gpus=0,
        gradient_clip_val=0.99,
        callbacks=[lr_logger, early_stop_callback],
        logger=neptune_logger
    )

    net = NBeats.from_dataset(
        training, learning_rate=3e-3, log_interval=10, log_val_interval=1, log_gradient_flow=False, weight_decay=1e-2,
        loss=RMSE()
    )

    net.hparams.log_interval = -1
    net.hparams.log_val_interval = 200

    trainer.fit(
        net, train_dataloader=train_dataloader,
        val_dataloaders=val_dataloader,
    )

    new_loader = TimeSeriesDataSet.from_dataset(training, dataset,
                                                min_prediction_idx=0,
                                                stop_randomization=True)

    best_model_path = trainer.checkpoint_callback.best_model_path
    best_net = NBeats.load_from_checkpoint(best_model_path)

    trainer.test(model=best_net, test_dataloaders=test_dataloader)
    plot_interval_predictions(best_net, new_loader, context_length, prediction_length, neptune_logger)

    # test_model(net, trainer, dataset, test_dataloader, training, neptune_logger, context_length, prediction_length)

if __name__ == '__main__':
    main()
