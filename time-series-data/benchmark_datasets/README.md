# Time series prediction - benchmark datasets and results comparision

## Benchmark datasets 

1. FCR (secure-document)
- benchmark_datasets/secure-document/deployment-reconfiguration-range-1-to-1/ (without reconfiguration)
- benchmark_datasets/secure-document/deployment-reconfiguration-range-1-to-20/ (with reconfiguration)

Metrics to predict: AvgResponseTimeTable (AvgResponseTimeTable.csv, value column), Requests (jmeterResults.csv, timeStamp column)

2. Genome 
- benchmark_datasets/deployment-reconfiguration-range-1-to-1 (without reconfiguration)
- benchmark_datasets/deployment-reconfiguration-range-1-to-10 (with reconfiguration)

metrics to predict: metrics from dataseset.csv (already preprocessed with dataset_maker.py)


Each dataset is divided into train and test (ratio 8:2). Information about the split can be found in 'split' column. It's allowed to use different genome e.g. whole datasets.csv (including test) from deployment-reconfiguration-range-1-to-1/2021-01-21 to 2021-01-22 to train model and report predictions metrics on test set from other dataset e.g. deployment-reconfiguration-range-1-to-1/2021-02-12 to 2021-02-13.

## Evaluation

Metrics: to calculate: MAE, MSE, MAPE, SMAPE










