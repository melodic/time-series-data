import pandas as pd 

# DATA_PATHS = ['AvgResponseTimeTable.csv', 'jmeterResults.csv', 'data.csv']
# DATA_PATHS = 'AvgResponseTimeTable.csv'

DATA_PATHS = 'data.csv'
data = pd.read_csv(DATA_PATHS)

n = data.shape[0]
data['split'] = 'train'
data['split'].iloc[int(n * 0.9):] = 'test'


# data = pd.read_csv(DATA_PATHS)
# req = pd.read_csv('jmeterResults.csv')

# n = data.shape[0]
# data['split'] = 'train'
# data['split'].iloc[int(n * 0.9):] = 'test'

# dt = pd.to_datetime(data[data.split == 'test']['time'].values[0]).tz_localize(None)

# req['split'] = req['timeStamp'].apply(lambda x: 'train' if pd.to_datetime(x).tz_localize(None) < dt else 'test')

# data.to_csv('AvgResponseTimeTable.csv')
# req.to_csv('jmeterResults.csv')


