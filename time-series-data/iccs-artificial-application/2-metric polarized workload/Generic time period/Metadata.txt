Workload for the ICCS artificial application. It is a sharp 'on-off' workload for 2 metric resources. One worker is assumed to be able to service up to 100 percent per resource. Numbers over 100 mean that the workload cannot be serviced using only one worker.

The individual workload curves are based on the following (numpy-based) definitions:

y1 = load_multiplier*(np.sign(np.sin(2*np.pi*x/100))+1)
y2 = load_multiplier*(np.sign(np.sin(np.pi/3+2*np.pi*x/100))+1)

The time period associated with each value pair is arbitrary - if a radically changing workload is desired then the time between two events can be assumed to be lower than 100msec.

The file contains rows with the following data:​

Attribute 1: A percentage-based metric (e.g CPU consumption)
Attribute 2: A percentage-based metric (e.g RAM consumption)