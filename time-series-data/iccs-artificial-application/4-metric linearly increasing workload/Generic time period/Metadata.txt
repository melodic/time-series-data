Workload for the ICCS artificial application. It is a simple workload which is assumed to reflect linearly increasing needs for 4 metric resources. One worker is assumed to be able to service up to 100 percent per resource. Numbers over 100 mean that the workload cannot be serviced using only one worker.

The time period associated with each value pair is arbitrary - if a radically changing workload is desired then the time between two events can be assumed to be lower than 100msec.

The file contains rows with the following data:​

Attribute 1: A percentage-based metric (e.g CPU consumption)
Attribute 2: A percentage-based metric (e.g RAM consumption)
Attribute 3: A percentage-based metric (e.g Hard disk space consumption)
Attribute 4: A percentage-based metric (e.g Network bandwidth consumption)
