# Time series prediction - results

    * predictions are saved with format:  {METRIC_NAME}_predictions/{METRIC_NAME}__predictions_{MODEL_NAME}_model{MODEL_VERSION}_{PREDICTION_STEP}.csv 

    MODEL_NAME e.g. tft, nbeats 
    
    PREDICTION_STEP - ranges from 0 to prediction_length - 1

    MODEL_VERSION - integer

    Predictions csv fields: 

    - preds (prediction)
    - step
    - time

Results (old)

1. FCR - average response time
  * fcr_2020-10-29 to 2020-10-30
    * time series 
    ![Alt text](images/fcr_2020-10-29 to 2020-10-30_ts.png?raw=true "Title")
    Average spike length: 151 seconds
    Median spike length: 120.0 seconds (2 points)
    * prediction (tft)
    ![Alt text](images/fcr_2020-10-29 to 2020-10-30_1_preds.png?raw=true "Title")
    * prediction with fake added points (2s)(tft)
    ![Alt text](images/fcr_fake_points.png?raw=true "Title")       

  * fcr_2020-11-27 to 2020-11-27
    * time series 
    ![Alt text](images/fcr_2020-11-27 to 2020-11-27_1.png?raw=true "Title")
    Average spike length: 377 seconds
    Median spike length: 180.0 seconds
    * prediction (tft)
    ![Alt text](images/fcr_2020-11-27 to 2020-11-27_1_preds_res.png?raw=true "Title")  
      
  * fcr_2020-12-09 to 2020-12-09
    * time series 
    ![Alt text](images/fcr_2020-12-09 to 2020-12-09_1.png?raw=true "Title")
    Average peak length is 10.57 seconds
    Median peak length is 10 seconds (2 points)
    * prediction
    ![Alt text](images/fcr_2020-12-09 to 2020-12-09_1_preds.png?raw=true "Title")
  * fcr_2020-12-15 to 2020-12-15
    * time series 
    ![Alt text](images/fcr_2020-12-15 to 2020-12-15_1.png?raw=true "Title")
    Average peak length is 10.67 seconds
    Median peak length is 10 seconds (2 points)
      
    ![Alt text](images/fcr_2020-12-15 to 2020-12-15_1_preds.png?raw=true "Title")


  * fcr_2021-01-18 to 2021-01-19
    * TODO
  * comments
    * predictions are saved with format:  predictions_{METHOD_NAME}.csv 
    * fcr average repsonse time - forecast with 1 min horizon
    * fcr average response time series is aggregated over 60 seconds  
    * TODO
    

2. FCR - requests
  * fcr_2020-10-29 to 2020-10-30 
    * prediction (tft)
      * 1 minute
      ![Alt text](images/fcr_2020-10-29 to 2020-10-30_1_preds_0.png?raw=true "Title")
      * 2 minutes  
      ![Alt text](images/fcr_2020-10-29 to 2020-10-30_1_preds_res.png?raw=true "Title")
      * 3 minutes   
      ![Alt text](images/fcr_2020-10-29 to 2020-10-30_1_preds_2.png?raw=true "Title")
    * prediction (n-beats)
      * 1 minute
      ![Alt text](images/nbeats_pred_0.png?raw=true "Title")
      * 2 minutes  
      ![Alt text](images/nbeats_pred_1.png?raw=true "Title")  
      * 3 minutes         
      ![Alt text](images/nbeats_pred_2.png?raw=true "Title") 
        
    * experiments with ensembles
      25 models (n-betas, tft) with different: predictions length, context length,
      loss functions (rmse, mae, dilate, quantileloss), model architecture
      * simple lasso regression (averaged 5 minutes prediction)
      ![Alt text](images/preds_ens_lasso.png?raw=true "Title")  
      * lgbm (overfitting)  (averaged 5 minutes prediction)    
      ![Alt text](images/preds_ens_lgbm.png?raw=true "Title") 

    * prediction (es-rnn)
      TODO     
  * comments
    * predictions for longer horizonts (egz. 10 minutes are also avaliable)
    * Requests were aggregated over 60 seconds
    * predictions are saved with format:  predictions_{METHOD_NAME}{STEP}.csv 
    * TODO

3. Genome
    * 2020-12-02 to 2020-12-02
        *  MinimumCoresContext prediction (tft)
           * 1 minute
          ![Alt text](images/genome_preds_0.png?raw=true "Title")
           * 2 minutes  
          ![Alt text](images/genome_preds_1.png?raw=true "Title")
           * 3 minutes          
          ![Alt text](images/genome_preds_2.png?raw=true "Title")
           * 4 minutes          
          ![Alt text](images/genome_preds_3.png?raw=true "Title")
           * 5 minutes            
          ![Alt text](images/genome_preds_4.png?raw=true "Title")
            
        *  MinimumCoresContext prediction (n-beats)
           * TODO
        *  MinimumCoresContext prediction (es-rnn)
           * TODO 
        
        * feature importance
          * ETPercentile
            
            ![Alt text](images/etpercentile_features.png?raw=true "Title")
          * MinimumCoresContext
            ![Alt text](images/minimumcorescontext_features.png?raw=true "Title")
        * Experiments with ensembles
          15 models with different: predictions length (5-20 min), context length,
          loss functions (rmse, mae, dilate, quantileloss), model architecture and metrics, 
          scores for single models are avaliable on neptune  
            * ETPercentile (averaged 5 minutes prediction)
              * simple lasso regression
                ![Alt text](images/ETPercentile_preds_ens_lasso.png?raw=true "Title")
              * lgbm (overfitting)
                ![Alt text](images/ETPercentile_preds_ens_lgbm.png?raw=true "Title")
            * MinimumCoresContext (averaged 5 minutes prediction)
              * simple lasso regression
                ![Alt text](images/preds_ens_lasso_mini.png?raw=true "Title")
    


  

