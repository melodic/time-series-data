# time-series-data
#Prepare an unified dataset

Datasets collected from InfluxDB are stored in different files
according to the time serie.
Ex.: 
MinimumCoresContext.csv for MinimumCoresContext metric
EstimatedRemainingTimeContext.csv for EstimatedRemainingTimeContext
where each has name, time, countryCode, ipAddress, level, producer, value

Dataset maker located to the prepare_unified_dataset folder allows combining selected files
into an unique dataset.
For the selected files "MinimumCoresContext.csv, EstimatedRemainingTimeContext.csv"
The python script will produce "dataset.csv" with the following features
time, MinimumCoresContext, EstimatedRemainingTimeContext

The line files = ['./EstimatedRemainingTimeContext.csv', './SimulationLeftNumber.csv', './SimulationElapsedTime.csv', './NotFinishedOnTime.csv', './MinimumCoresContext.csv', './NotFinished.csv', './WillFinishTooSoonContext.csv', './NotFinishedOnTimeContext.csv', './MinimumCores.csv', './ETPercentile.csv', './RemainingSimulationTimeMetric.csv', './TotalCores.csv']

defines the selected files, change it according to your need.




